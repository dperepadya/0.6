package com.nmu.perepadya.lab1.lab2;

/**
 * Created by 49 on 13.11.2016.
 */
import com.nmu.model.Description;

public class Lab2 {
    public static void main(String[] args) {

        Description description = new Description();
        description.starts();

        int arrayCount = 5;
        int[][] massive = new int[arrayCount][arrayCount];
        for(int i=0;i<arrayCount;i++){
            for(int j=0;j<arrayCount;j++){
                massive[i][j] = ((int)Math.round(Math.random()*10));
            }
        }
        for (int i=0;i<arrayCount;i++,System.out.println()){
            for(int j=0;j<arrayCount;j++){
                System.out.print(massive[i][j]+" ");
            }
        }

        description.calculate();

        int min = massive[0][0];
        for(int i=0; i<arrayCount-1; i++){
            for(int j=0; j<arrayCount; j++){
                if(massive[i][j]<min){
                    min = massive[i][j];
                }
            }
        }
        int max = massive[0][0];
        for(int i=0; i<arrayCount-1; i++){
            for(int j=0; j<arrayCount; j++){
                if(massive[i][j]>max){
                    max = massive[i][j];
                }
            }
        }

        System.out.println("Min="+min);
        System.out.println("Max="+max);

        String say = description.speak("Конец лабы");
        System.out.println(say);
    }
}

