package com.nmu.model;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * Created by 49 on 16.10.2016.
 */
public class DescriptionTest extends TestCase {
    @Test
    public void testSpeak() throws Exception {
        Description description=new Description();
        String say=description.speak("Конец лабы");
        assertEquals("Конец лабы...приглушенные аплодисменты...\n",say);
    }
}