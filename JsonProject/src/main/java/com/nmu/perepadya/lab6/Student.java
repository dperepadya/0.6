package com.nmu.perepadya.lab6;

import static java.lang.String.*;

/**
 * Created by 49 on 01.12.2016.
 */
public class Student extends Group{
    public String name;
    public String lastName;
    public Integer age;
    public Boolean isTheElderGroup;
    public Student (String name, String lastName,Integer age,Boolean isTheElderGroup, String facultyName, String departmentName, String groupName, Integer year){
        this.name=name;
        this.lastName=lastName;
        this.age=age;
        this.isTheElderGroup=isTheElderGroup;
        this.facultyName=facultyName;
        this.departmentName=departmentName;
        this.groupName=groupName;
        this.year=year;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        if (!super.equals(o)) return false;

        Student student = (Student) o;

        if (!name.equals(student.name)) return false;
        if (!lastName.equals(student.lastName)) return false;
        if (!age.equals(student.age)) return false;
        return isTheElderGroup.equals(student.isTheElderGroup);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + age.hashCode();
        result = 31 * result + isTheElderGroup.hashCode();
        return result;
    }
        public String toString(){

            return format("%s : %s : %s : %s : %s : %s : %s : %s", name, lastName, age,isTheElderGroup, facultyName, departmentName, groupName, year);
        }


}
