package com.nmu.perepadya.lab6;

/**
 * Created by 49 on 01.12.2016.
 */
public class Group extends Department{
    public String groupName;
    public Integer year;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;

        Group group = (Group) o;

        if (!groupName.equals(group.groupName)) return false;
        return year.equals(group.year);

    }

    @Override
    public int hashCode() {
        int result = groupName.hashCode();
        result = 31 * result + year.hashCode();
        return result;
    }
}
