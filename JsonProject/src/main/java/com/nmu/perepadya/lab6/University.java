package com.nmu.perepadya.lab6;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 * Created by 49 on 01.12.2016.
 */
public class University {
    private static final String FILENAME= "C:\\Users\\49\\IdeaProjects\\JsonProject\\file.json";
    public static void main(String args[]){
        /*Faculty faculty=new Faculty();
        Department department=new Department();
        Group group=new Group();
        Student student=new Student();*/

        /*faculty.facultyName="Faculty of Information Technology";
        department.departmentName="Department of System Analysis and Management";
        group.groupName="SAit";
        group.year=2013;*/
        /*student.facultyName="Faculty of Information Technology";
        student.departmentName="Department of System Analysis and Management";
        student.groupName="SAit";
        student.year=2013;
        student.name="Diana";
        student.lastName="Perepadya";
        student.age=20;
        student.isTheElderGroup=false;

        Gson gson=new Gson();
        String json = gson.toJson(student);
        System.out.println(json);
        Student fromJsonStudent=new Student();
        fromJsonStudent=gson.fromJson(json,Student.class);
        System.out.println(student.equals(fromJsonStudent));*/



        List<Student> listItems = new ArrayList<Student>();
        listItems.add( new Student("Diana", "Perepadya", 20, false, "Faculty of Information Technology", "Department of System Analysis and Management", "SAit",2013));
        listItems.add( new Student("Smbd" , "Smbd", 17, false, "Electrical Engineering Faculty", "Department of Power-Supply systems", "PSSee",2016));
        listItems.add( new Student("Smbd" , "Smbd", 18, true, "Faculty of management", "Department of Management of production sphere", "MPSm",2015));
        listItems.add( new Student("Smbd" , "Smbd", 18, true, "Faculty of finances and economics", "Department of economic cybernetics and information technologies", "CITfe",2015));
        listItems.add( new Student("Smbd" , "Smbd", 18, true, "Mining faculty", "Department of Ecology", "Em",2015));
        listItems.add( new Student("Smbd" , "Smbd", 18, false, "Faculty of construction", "Department of Geodesy", "Gc",2015));
        listItems.add( new Student("Smbd" , "Smbd", 22, false, "Faculty of Mechanical Engineering", "Department of Structural, Engineering and Applied Mechanics", "SEAMme",2012));
        listItems.add( new Student("Smbd" , "Smbd", 18, true, "Faculty of Geological Prospecting", "Department of Chemistry", "Cgp",2015));
        listItems.add( new Student("Smbd" , "Smbd", 18, true, "Faculty of Law", "Department of Civil and Economic Law", "CELl",2015));

        String jsonStr = new Gson().toJson(listItems);
        System.out.println(jsonStr);
        Type itemsListType = new TypeToken<List<Student>>() {}.getType();
        List<Student> fromJsonStudent = new Gson().fromJson(jsonStr,itemsListType);
        System.out.println(fromJsonStudent.toString());
        System.out.println(listItems.equals(fromJsonStudent));

        try (FileWriter writer = new FileWriter(FILENAME)){
            writer.write(jsonStr);
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(University.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

    }
}
