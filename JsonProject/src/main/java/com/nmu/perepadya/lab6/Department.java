package com.nmu.perepadya.lab6;

/**
 * Created by 49 on 01.12.2016.
 */
public class Department extends Faculty{
    public String departmentName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;

        Department that = (Department) o;

        return departmentName.equals(that.departmentName);

    }

    @Override
    public int hashCode() {
        return departmentName.hashCode();
    }
}
