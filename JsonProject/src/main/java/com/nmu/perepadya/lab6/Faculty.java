package com.nmu.perepadya.lab6;

/**
 * Created by 49 on 01.12.2016.
 */
public class Faculty {
    public String facultyName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Faculty)) return false;

        Faculty faculty = (Faculty) o;

        return facultyName.equals(faculty.facultyName);

    }

    @Override
    public int hashCode() {
        return facultyName.hashCode();
    }
}
